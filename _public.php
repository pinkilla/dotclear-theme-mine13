<?php

if (!defined('DC_RC_PATH')) { return; }

$core->tpl->addValue('EntryFewWords',array('tplEntryFewWords','EntryFewWords'));

/**
* 
*/
class tplEntryFewWords 
{
	
	public static function EntryFewWords($attr)
	{
		// Je récupère le contexte et le core
		global $_ctx, $core;
        
        $urls = '0';
        if (!empty($attr['absolute_urls'])) {
            $urls = '1';
        }
       
        if ( !isset($attr['cut_string'])) {
            $attr['cut_string'] = '200';
        }        
        $attr['remove_html'] = '1';         
        $f = $core->tpl->getFilters($attr);

        return '<?php echo '.sprintf($f,
                '$_ctx->posts->getExcerpt('.$urls.')." ".$_ctx->posts->getContent('.$urls.')').'; echo " (…)"; ?>';		              
                
	}
}
?>