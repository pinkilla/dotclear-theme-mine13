# Thème Mine13
Un thème qui se veut minimaliste en une colonne, sans sidebar et sans aucune place pour les widgets. 

Le thème est mon essai de faire un site responsive sur base de [Knacss][] … sans utilisation d'un préprocesseur ce qui fait que mon usage n'est sans doute pas le meilleur (on en discute d'ailleurs un peu dans ce [billet][]). 

Vous pouvez l'utiliser, mais ne conservez pas l'icone en forme de pied et faites les changements proposés dans la partie installation

##Installation
Le thème s'installe à partir du zip comme tout autre thème mais nécéssite de faire un peu de ménage. 

- Changer l'icone en forme de pied, c'est le fichier `img/icon.png` (vous pouvez utiliser la petite maison verte)
- modifier le fichier tpl`_social.html` pour y renseigner vos « valeurs sociales »; les liens dont vous avez envie, … 
- modifier la signature des billets dans la page`tpl/post.html`

Nécessite le plugin [Gravatar][]

## Todos
- 404.html 
- Trouver la page appelée comme résultat d'une rechercre … et la mettre à jour
- Ajouter un template pour la signature qu'il suffira de mettre à jour 

##Auteur
PiT / Pinkilla · <pk@namok.be> · (http.//pit.namok.be)

J'apprécie d'être informé si tu utilises le thème … simplement pour me *toucher un peu les têtons*.

- v0.0.4 	Suppression de l'anonymisation et ajout des liens de partage sociaux 
			« sans tracking »
			Ajout d'un screeshot \°/
- v0.0.3 	Anonymisation légère et mal faite au cas où quelqu'un utiliserait
- v0.0.2 	Deuxième version, un peu plus utilisable
- vO.O.1 	Première version quasi fonctionnelle

##Crédits
- Icon by Thomas Hirter du [The Noun Project][]
- [Knacss] pour la feuille de style responsive
- Ce site (?) expliqauant commen utiliser une police de caractère pour gérer les icônes
- …


[Gravatar]: http://plugins.dotaddict.org/dc2/details/gravatar
[Knacss]: http://knacss.com/
[billet]: http://namok.be/blog/?post/2013/09/23/mine13-theme-dotclear
[The Noun Project]: http://thenounproject.com/noun/footprint/#icon-No4820

